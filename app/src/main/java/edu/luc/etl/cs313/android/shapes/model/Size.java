package edu.luc.etl.cs313.android.shapes.model;

/**
 * A visitor to compute the number of basic shapes in a (possibly complex)
 * shape.
 */
public class Size implements Visitor<Integer> {

	@Override
	public Integer onPolygon(final Polygon p)
	{
		int pCount = 1;
		for(Point points : p.getPoints()) {
			pCount += points.getShape().accept(this);
		}
		return 1;
	}

	@Override
	public Integer onCircle(final Circle c) //DONE
	{
		if(c.getRadius() != 0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	@Override
	public Integer onGroup(final Group g) //DONE
	{
		int groupCount = 0;
		for(Shape s : g.getShapes())
		{
			groupCount += s.accept(this);
		}
		return groupCount;
	}

	@Override
	public Integer onRectangle(final Rectangle q)
	{
		return 1;
	}

	@Override
	public Integer onOutline(final Outline o)
	{
			return o.getShape().accept(this);
	}

	@Override
	public Integer onFill(final Fill c)
	{
			return c.getShape().accept(this);
	}

	@Override
	public Integer onLocation(final Location l)
	{
			return l.getShape().accept(this);
	}

	@Override
	public Integer onStroke(final Stroke c)
	{
			return c.getShape().accept(this);
	}
}
