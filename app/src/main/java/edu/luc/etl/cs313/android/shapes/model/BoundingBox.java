package edu.luc.etl.cs313.android.shapes.model;
import java.util.*;
/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f)
	{
		return f.getShape().accept(this);
	}


	@Override
	public Location onGroup(final Group g)
	{
		List<Integer> xVal = new ArrayList();
		List<Integer> yVal = new ArrayList();


		for(Shape s : g.getShapes())
		{
			Location loc = s.accept(this);
			int x = loc.getX();
			int y = loc.getY();
			Rectangle r = (Rectangle)loc.getShape();
			int width = r.getWidth();
			int height = r.getHeight();
			xVal.add(x);
			xVal.add(width);
			yVal.add(y);
			yVal.add(height);
		}
		return new Location(Collections.min(xVal), Collections.max(yVal), new Rectangle((Collections.max(xVal)) - (Collections.min(xVal)), ((Collections.max(yVal))-(Collections.min(yVal)))));
	}

	@Override
	public Location onLocation(final Location l)
	{
		return new Location(l.getX(), l.getY(), l.getShape().accept(this).getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r)
	{
		return new Location(0, 0, new Rectangle(r.getWidth(), r.getHeight()));
	}

	@Override
	public Location onStroke(final Stroke c)
	{
		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o)
	{
		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s)
	{
		List<Integer> xVal = new ArrayList();
		List<Integer> yVal = new ArrayList();

		for(Point p : s.getPoints())
		{
			xVal.add(p.getX());
			yVal.add(p.getY());
		}

		return new Location(Collections.min(xVal), Collections.min(yVal), new Rectangle(Collections.max(xVal)-Collections.min(xVal), Collections.max(yVal)-Collections.min(yVal)));
	}
}
